<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Test\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{
    public function indexAction()
    {
        return new ViewModel();
    }

    public function testAction()
    {
    }
    public function searchAction()
    {
        $map = '{
            "html_attributions" : [],
            "next_page_token" : "CqQCEwEAABh-1DHkjfreeSUCPWcpCy_4pXtgtMy9Z41HjN-9B9Ni0Q0tTBfsrz9Ed8cdWMfb9ueLFM5cXxLPT1iaeKSaR5UOXoMBgttQUwfR5L6g0fWUpTZMOSd31Dj2tCAts1VZMwyhGFeNI05Z3GwQoFviOH8MJ_IpjZ89p_rQizIKuZLRDVcFemDRnFFQRbzTaWeP7k_6a_XDOYo57KZmIoKyk2t7b-scyDrrFBXYuOvTbQ1V0SGPMnZDSHnaQIO_BoL4hYRq8GqgeGaspWOAl2wnDl1dBVPqkD6G40CjsvlmNxMmnJxAiVkDBA2lFOPKzne8c6Nxz1Wvi_ByuT1CsjOsYG3B0Pbc-OIPxPvJTtHQl01_5eNYCPlZ2zLwYgi-dS7DeBIQFnNp2NhAkxBUmzFwr6DFKxoUT1u0Z4bPtONs6G_ir5pwCgx4aF8",
            "results" : [
               {
                  "formatted_address" : "662 Rd, เตชะวานิช แขวง บางซื่อ เขตบางซื่อ กรุงเทพมหานคร 10800 ไทย",
                  "geometry" : {
                     "location" : {
                        "lat" : 13.802348,
                        "lng" : 100.534942
                     },
                     "viewport" : {
                        "northeast" : {
                           "lat" : 13.80373022989272,
                           "lng" : 100.5362260798927
                        },
                        "southwest" : {
                           "lat" : 13.80103057010728,
                           "lng" : 100.5335264201073
                        }
                     }
                  },
                  "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
                  "id" : "9d1e0492e2153404590409c04b24465a6aad9f2f",
                  "name" : "Kumamura Food.Bar",
                  "opening_hours" : {
                     "open_now" : true
                  },
                  "photos" : [
                     {
                        "height" : 3000,
                        "html_attributions" : [
                           "\u003ca href=\"https://maps.google.com/maps/contrib/100447619971491304233/photos\"\u003eSuthiluck Tangsutthimongkhon\u003c/a\u003e"
                        ],
                        "photo_reference" : "CmRaAAAApo7feIp4ZfJDAlysBO_NI7Y5Si5aGALIwHFTTZkNqA7tyyfBgaQEJGlwwZsupRU8JyK4VKEnPSMb1pyQAw6VfVU5R8-XAeJNFAQpbYxIqGq9bNUCoXmzJK3TdDm1AfyQEhCTuZz2JBf9VjxBC0weFA-1GhQMKTvNp9OiGSTfICBuMjG2mfvjPA",
                        "width" : 4000
                     }
                  ],
                  "place_id" : "ChIJdWfiWQuc4jARi2oLVe-QNGs",
                  "plus_code" : {
                     "compound_code" : "RG2M+WX กรุงเทพมหานคร",
                     "global_code" : "7P52RG2M+WX"
                  },
                  "rating" : 4.2,
                  "reference" : "ChIJdWfiWQuc4jARi2oLVe-QNGs",
                  "types" : [ "restaurant", "food", "point_of_interest", "establishment" ],
                  "user_ratings_total" : 97
               },
               {
                  "formatted_address" : "9 11 ถนน เทอดดำริห์ แขวง บางซื่อ เขตบางซื่อ กรุงเทพมหานคร 10800 ประเทศไทย",
                  "geometry" : {
                     "location" : {
                        "lat" : 13.8020812,
                        "lng" : 100.5387946
                     },
                     "viewport" : {
                        "northeast" : {
                           "lat" : 13.80340652989272,
                           "lng" : 100.5402081298927
                        },
                        "southwest" : {
                           "lat" : 13.80070687010728,
                           "lng" : 100.5375084701073
                        }
                     }
                  },
                  "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
                  "id" : "72cfed271968a2f11b4d2101eb120a414675d252",
                  "name" : "Bella Casa",
                  "opening_hours" : {
                     "open_now" : true
                  },
                  "photos" : [
                     {
                        "height" : 3024,
                        "html_attributions" : [
                           "\u003ca href=\"https://maps.google.com/maps/contrib/109111240858438466286/photos\"\u003eMitree Prasanatikom\u003c/a\u003e"
                        ],
                        "photo_reference" : "CmRZAAAA9gCTyXCGPGLKOdolhBxQSQn1giSYXffJ7pbvsNd9mVr6BgwusdUlqbyk3XHHHUb8GXaummaQFy3Ls-m1k2lKUFQ7lLdayeV9LoKlfNU2MubCDI-zDEbbK8xwuLpEQKoMEhA10huIDBu-DE_bMGuZcFWWGhQSPnKC0GfN0Oy99-aS_XxgLT8xrA",
                        "width" : 4032
                     }
                  ],
                  "place_id" : "ChIJpQIYOw2c4jARAuLothgZn9Y",
                  "plus_code" : {
                     "compound_code" : "RG2Q+RG กรุงเทพมหานคร",
                     "global_code" : "7P52RG2Q+RG"
                  },
                  "price_level" : 2,
                  "rating" : 4.2,
                  "reference" : "ChIJpQIYOw2c4jARAuLothgZn9Y",
                  "types" : [ "restaurant", "food", "point_of_interest", "establishment" ],
                  "user_ratings_total" : 247
               },
               {
                  "formatted_address" : "162, 1 ถนน ประชาราษฎร์ สาย 2 แขวง บางซื่อ เขตบางซื่อ กรุงเทพมหานคร 10800 ประเทศไทย",
                  "geometry" : {
                     "location" : {
                        "lat" : 13.8052051,
                        "lng" : 100.5241228
                     },
                     "viewport" : {
                        "northeast" : {
                           "lat" : 13.80654687989272,
                           "lng" : 100.5256689298927
                        },
                        "southwest" : {
                           "lat" : 13.80384722010728,
                           "lng" : 100.5229692701073
                        }
                     }
                  },
                  "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
                  "id" : "376ef2cf6ae0e24b18cd184b9cd58643c68823f1",
                  "name" : "MK Restaurant-Gateway Bangsue",
                  "opening_hours" : {
                     "open_now" : true
                  },
                  "photos" : [
                     {
                        "height" : 1365,
                        "html_attributions" : [
                           "\u003ca href=\"https://maps.google.com/maps/contrib/117840845425636014645/photos\"\u003eBIGSHOW LOWLAR\u003c/a\u003e"
                        ],
                        "photo_reference" : "CmRaAAAAthvZ7T93qLvdsSFQBjQTxXgzV9MshaScBU7mVQs8DQrYI9fuiemWPwxyyDXn8-pRMpr4cvjQ7kKNq9z8rrPsPNrrCQZRulv71teqt5ViCu2FrquR1wyG_RUnZa7UQtCuEhBkEbqu-eybmaNUSJ8MnZs7GhTNHVU5Xt7oZpf6PdzPAW6qwIhOKw",
                        "width" : 2048
                     }
                  ],
                  "place_id" : "ChIJdcdrnIyb4jARidxLOO6zAZs",
                  "plus_code" : {
                     "compound_code" : "RG4F+3J กรุงเทพมหานคร",
                     "global_code" : "7P52RG4F+3J"
                  },
                  "price_level" : 2,
                  "rating" : 4.1,
                  "reference" : "ChIJdcdrnIyb4jARidxLOO6zAZs",
                  "types" : [ "restaurant", "food", "point_of_interest", "establishment" ],
                  "user_ratings_total" : 23
               },
               {
                  "formatted_address" : "105 ซอย ประชาราษฎร์ 1 แขวง บางซื่อ เขตบางซื่อ กรุงเทพมหานคร 10800 ประเทศไทย",
                  "geometry" : {
                     "location" : {
                        "lat" : 13.8026026,
                        "lng" : 100.521231
                     },
                     "viewport" : {
                        "northeast" : {
                           "lat" : 13.80394937989272,
                           "lng" : 100.5226460298927
                        },
                        "southwest" : {
                           "lat" : 13.80124972010728,
                           "lng" : 100.5199463701073
                        }
                     }
                  },
                  "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
                  "id" : "b28a6eaa3e5a8e7b4ec0b036c451509d0ee50c5b",
                  "name" : "โกอ่างโภชนา สวนสน",
                  "opening_hours" : {
                     "open_now" : true
                  },
                  "photos" : [
                     {
                        "height" : 3024,
                        "html_attributions" : [
                           "\u003ca href=\"https://maps.google.com/maps/contrib/114854983772325038940/photos\"\u003eWatcharin Buason\u003c/a\u003e"
                        ],
                        "photo_reference" : "CmRaAAAADRsflD5xDUipXE4BfvVDy1qWVlMNpN7syX74ZHE-yLxpZ2idZjS_xLDCFtgovwBxUlrD5nyrktxab4iOdiePh_XdPI4tr-csK-xWeGcQ5cP3jMVaCT8CJ2k-QCgSdvChEhDL5Wsddb_8Qwt34B8fzSxnGhQVH78v3Gw5HeftOExr9o6m1IiSRg",
                        "width" : 4032
                     }
                  ],
                  "place_id" : "ChIJlUlMJ_Ob4jARLF8cfRQRkVA",
                  "plus_code" : {
                     "compound_code" : "RG3C+2F กรุงเทพมหานคร",
                     "global_code" : "7P52RG3C+2F"
                  },
                  "rating" : 4.1,
                  "reference" : "ChIJlUlMJ_Ob4jARLF8cfRQRkVA",
                  "types" : [ "restaurant", "food", "point_of_interest", "establishment" ],
                  "user_ratings_total" : 87
               },
               {
                  "formatted_address" : "309 ถนนประชาราษฎร์สาย1 แขวง บางซื่อ เขตบางซื่อ กรุงเทพมหานคร 10800 ประเทศไทย",
                  "geometry" : {
                     "location" : {
                        "lat" : 13.8059395,
                        "lng" : 100.5213903
                     },
                     "viewport" : {
                        "northeast" : {
                           "lat" : 13.80728002989272,
                           "lng" : 100.5228385298927
                        },
                        "southwest" : {
                           "lat" : 13.80458037010728,
                           "lng" : 100.5201388701073
                        }
                     }
                  },
                  "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
                  "id" : "e930b928046f981f79b9291c9b57b707a8e1af2c",
                  "name" : "วิเศษไก่ย่างภัตตาคาร",
                  "opening_hours" : {
                     "open_now" : true
                  },
                  "photos" : [
                     {
                        "height" : 2268,
                        "html_attributions" : [
                           "\u003ca href=\"https://maps.google.com/maps/contrib/109367997604945871356/photos\"\u003eH2OSolidJitrawee\u003c/a\u003e"
                        ],
                        "photo_reference" : "CmRaAAAA0oSWsP06PK6wiQ2K5selx6P8EKsj4kQrYZ4ZXQPCqTklOa9EAnr0UJ_r-742Y6GTYdi_KJ_CV6hLDB2RdahCosIaHZYmd_CkuRc8I5Jv0M47IXy0Gitba6ermtwWlKpmEhB9cBP8pYq55awJeTN22Kz6GhRv69Y7vxr3PO7ZA52DcLS09fqnoQ",
                        "width" : 4032
                     }
                  ],
                  "place_id" : "ChIJjY76_Iyb4jARZMDqSmOkMqg",
                  "plus_code" : {
                     "compound_code" : "RG4C+9H กรุงเทพมหานคร",
                     "global_code" : "7P52RG4C+9H"
                  },
                  "price_level" : 2,
                  "rating" : 4.1,
                  "reference" : "ChIJjY76_Iyb4jARZMDqSmOkMqg",
                  "types" : [ "restaurant", "food", "point_of_interest", "establishment" ],
                  "user_ratings_total" : 772
               },
               {
                  "formatted_address" : "ประชาราษฎร์ สาย 1 ซอย 20 แขวง บางซื่อ เขตบางซื่อ กรุงเทพมหานคร 10800 ประเทศไทย",
                  "geometry" : {
                     "location" : {
                        "lat" : 13.807439,
                        "lng" : 100.521951
                     },
                     "viewport" : {
                        "northeast" : {
                           "lat" : 13.80882917989272,
                           "lng" : 100.5232994798927
                        },
                        "southwest" : {
                           "lat" : 13.80612952010728,
                           "lng" : 100.5205998201073
                        }
                     }
                  },
                  "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
                  "id" : "c2a76ebd669cd7eba2cf384af32c1c32ee39c69a",
                  "name" : "โตเป็ดย่าง",
                  "opening_hours" : {
                     "open_now" : true
                  },
                  "photos" : [
                     {
                        "height" : 2048,
                        "html_attributions" : [
                           "\u003ca href=\"https://maps.google.com/maps/contrib/110793402542821154351/photos\"\u003eโตเป็ดย่าง\u003c/a\u003e"
                        ],
                        "photo_reference" : "CmRaAAAA_VF2qefkk4-47X4t35f_u6CahrNpY7A3mJYY_RIzfx9fOwXTYBblL7hEJTfnx9n2YFE1qJOIfa73t6lFTcYWMR34ZBhxvBY2vLtbTUs5BnFAcYN_iiVT9SkGTIPeLZbgEhA3zurr-nk6U1GITj6votbtGhRs0-cR3gqpFHOlDI0VxdKI6IvZ1g",
                        "width" : 1536
                     }
                  ],
                  "place_id" : "ChIJxya9RYyb4jARKrIqfI_hDes",
                  "plus_code" : {
                     "compound_code" : "RG4C+XQ กรุงเทพมหานคร",
                     "global_code" : "7P52RG4C+XQ"
                  },
                  "price_level" : 2,
                  "rating" : 4.4,
                  "reference" : "ChIJxya9RYyb4jARKrIqfI_hDes",
                  "types" : [ "restaurant", "food", "point_of_interest", "establishment" ],
                  "user_ratings_total" : 83
               },
               {
                  "formatted_address" : "4 คอนโดยูดีไลท์ 2 Shop ปาก ซอย ประชาชื่น 19 แขวง บางซื่อ เขตบางซื่อ กรุงเทพมหานคร 10800 ประเทศไทย",
                  "geometry" : {
                     "location" : {
                        "lat" : 13.8189679,
                        "lng" : 100.535996
                     },
                     "viewport" : {
                        "northeast" : {
                           "lat" : 13.82029717989272,
                           "lng" : 100.5374884798927
                        },
                        "southwest" : {
                           "lat" : 13.81759752010728,
                           "lng" : 100.5347888201073
                        }
                     }
                  },
                  "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
                  "id" : "a4f47b733b39e8149988ceb25cafbc098c7139e8",
                  "name" : "Come Home Cafe ร้านอาหาร คำโฮม คาเฟ่",
                  "opening_hours" : {
                     "open_now" : true
                  },
                  "photos" : [
                     {
                        "height" : 3182,
                        "html_attributions" : [
                           "\u003ca href=\"https://maps.google.com/maps/contrib/111960096286926411983/photos\"\u003eparamaporn loiwattana\u003c/a\u003e"
                        ],
                        "photo_reference" : "CmRaAAAAnaBaXCELKQxBIsybtQ_2bc4HfAj7fIrBdz4mnpehei0wzxcqzJVXSWgJIpJbCfXH-Up9BC22ISnq_tqhlxlCA6i-n7hOu1UGN_sVcQV0y36dTDumCIYlLFIr27YnNeugEhB1TK8PPDW1B0ZAPJf-_5OBGhRJHwvZEkgtqfnfpZk8eNOzSRWYeQ",
                        "width" : 5546
                     }
                  ],
                  "place_id" : "ChIJ5-YV6Xuc4jARXI2YW2JufKw",
                  "plus_code" : {
                     "compound_code" : "RG9P+H9 กรุงเทพมหานคร",
                     "global_code" : "7P52RG9P+H9"
                  },
                  "rating" : 4.4,
                  "reference" : "ChIJ5-YV6Xuc4jARXI2YW2JufKw",
                  "types" : [
                     "restaurant",
                     "cafe",
                     "health",
                     "store",
                     "food",
                     "point_of_interest",
                     "establishment"
                  ],
                  "user_ratings_total" : 75
               },
               {
                  "formatted_address" : "แขวง บางซื่อ เขตบางซื่อ กรุงเทพมหานคร 10800 ประเทศไทย",
                  "geometry" : {
                     "location" : {
                        "lat" : 13.8064391,
                        "lng" : 100.5312434
                     },
                     "viewport" : {
                        "northeast" : {
                           "lat" : 13.80772372989272,
                           "lng" : 100.5325751798927
                        },
                        "southwest" : {
                           "lat" : 13.80502407010728,
                           "lng" : 100.5298755201073
                        }
                     }
                  },
                  "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
                  "id" : "4b027ce2ceb7d022707956388d982994edeb535e",
                  "name" : "P-cube Restaurant",
                  "opening_hours" : {
                     "open_now" : true
                  },
                  "photos" : [
                     {
                        "height" : 3120,
                        "html_attributions" : [
                           "\u003ca href=\"https://maps.google.com/maps/contrib/108617732229092501358/photos\"\u003ePichakorn Kongsuwan\u003c/a\u003e"
                        ],
                        "photo_reference" : "CmRaAAAAYfmrv6MCS7FbITlkDqV1FY6j2KQXVXccVO1pcB1TqVJXjJ0JgPZCbLW0OG6d3hWMWoxXm_kpj_CJRRBtKrvMY_z0p6PR_f-zlZzPVSFve-dN58gfhNSrnfNqys8umm6SEhDaueYorzydbYbp2_4PDU2pGhTiIUiL9XLa-SjiYVRUmEzv25EdXg",
                        "width" : 4160
                     }
                  ],
                  "place_id" : "ChIJE5VioXWc4jARptRjgTjXI68",
                  "plus_code" : {
                     "compound_code" : "RG4J+HF กรุงเทพมหานคร",
                     "global_code" : "7P52RG4J+HF"
                  },
                  "rating" : 4.8,
                  "reference" : "ChIJE5VioXWc4jARptRjgTjXI68",
                  "types" : [ "restaurant", "food", "point_of_interest", "establishment" ],
                  "user_ratings_total" : 4
               },
               {
                  "formatted_address" : "ถนน ริมคลองประปา ซอย สะพาน 99 แขวง บางซื่อ เขตบางซื่อ กรุงเทพมหานคร 10800 ประเทศไทย",
                  "geometry" : {
                     "location" : {
                        "lat" : 13.816952,
                        "lng" : 100.537218
                     },
                     "viewport" : {
                        "northeast" : {
                           "lat" : 13.81832642989272,
                           "lng" : 100.5385624298927
                        },
                        "southwest" : {
                           "lat" : 13.81562677010728,
                           "lng" : 100.5358627701073
                        }
                     }
                  },
                  "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
                  "id" : "3796e49f6210d26a9bf7a7160570ca0935b06b00",
                  "name" : "กาลิค",
                  "opening_hours" : {
                     "open_now" : true
                  },
                  "photos" : [
                     {
                        "height" : 1408,
                        "html_attributions" : [
                           "\u003ca href=\"https://maps.google.com/maps/contrib/113398794844342027560/photos\"\u003eEknarong Pholboonma\u003c/a\u003e"
                        ],
                        "photo_reference" : "CmRaAAAAZVnBUfir6OnFAqpOsjwljvQ-gwrf2nTGnbgzsmYjq13A9ohCo0oyFuGNk49oJ7IhK1cto9eX_LAobMmvJ-4iCBZw2PoDofryHjizo4VwQKaF-H6_a8O7BQ3tahxGeLoiEhBOzpJjG7LaElQHb-N5-2MBGhQRKVn_kwXnS56bP8a_P5CkcXyXlA",
                        "width" : 2496
                     }
                  ],
                  "place_id" : "ChIJf_7ra4Gc4jARLSdM7obyb9U",
                  "plus_code" : {
                     "compound_code" : "RG8P+QV กรุงเทพมหานคร",
                     "global_code" : "7P52RG8P+QV"
                  },
                  "price_level" : 2,
                  "rating" : 4.2,
                  "reference" : "ChIJf_7ra4Gc4jARLSdM7obyb9U",
                  "types" : [ "restaurant", "food", "point_of_interest", "establishment" ],
                  "user_ratings_total" : 379
               },
               {
                  "formatted_address" : "829, Tesco Lotus Prachachuen, Pracharat Sai 2 Road, Bang Sue, Khet Bang Sue, Bangkok, 10800, 10800 ไทย",
                  "geometry" : {
                     "location" : {
                        "lat" : 13.8060988,
                        "lng" : 100.533928
                     },
                     "viewport" : {
                        "northeast" : {
                           "lat" : 13.80747002989272,
                           "lng" : 100.5351816298927
                        },
                        "southwest" : {
                           "lat" : 13.80477037010728,
                           "lng" : 100.5324819701073
                        }
                     }
                  },
                  "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
                  "id" : "5e424016d85f433a4a3b315f54cb3c1a8760877f",
                  "name" : "เอ็มเค เรสโตรองต์",
                  "opening_hours" : {
                     "open_now" : true
                  },
                  "photos" : [
                     {
                        "height" : 3024,
                        "html_attributions" : [
                           "\u003ca href=\"https://maps.google.com/maps/contrib/118161780729145724847/photos\"\u003eSuporn Pongnumkul\u003c/a\u003e"
                        ],
                        "photo_reference" : "CmRaAAAAUxrlL256Ci9Ie9bgPf5oD2ATNW_C8cNPAM6Je7ytQD1nZZ7uXK1R0imbsiOEe7ATpOik7ZbKQ_b0HBret7vW4PZzJxhMzpmgQKaIHWwiyLaO41dl0tK0TtHD4iAeaV7REhBedrGSAawVQC9qfdRS_INYGhSrSEBBT0t7UjNHkYDKKRNQiJzjQA",
                        "width" : 4032
                     }
                  ],
                  "place_id" : "ChIJ_1dwfXSc4jAREdfKFF0OsJQ",
                  "plus_code" : {
                     "compound_code" : "RG4M+CH กรุงเทพมหานคร",
                     "global_code" : "7P52RG4M+CH"
                  },
                  "price_level" : 2,
                  "rating" : 3.7,
                  "reference" : "ChIJ_1dwfXSc4jAREdfKFF0OsJQ",
                  "types" : [ "restaurant", "food", "point_of_interest", "establishment" ],
                  "user_ratings_total" : 13
               },
               {
                  "formatted_address" : "9/242 ซอย รัชดาภิเษก 66 แขวง วงศ์สว่าง เขตบางซื่อ กรุงเทพมหานคร 10800 ประเทศไทย",
                  "geometry" : {
                     "location" : {
                        "lat" : 13.8309442,
                        "lng" : 100.5360147
                     },
                     "viewport" : {
                        "northeast" : {
                           "lat" : 13.83222697989272,
                           "lng" : 100.5373582798927
                        },
                        "southwest" : {
                           "lat" : 13.82952732010728,
                           "lng" : 100.5346586201073
                        }
                     }
                  },
                  "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
                  "id" : "66398955b6d2bdf32d2d8d84aa6271598e18cd2f",
                  "name" : "Buzza",
                  "opening_hours" : {
                     "open_now" : true
                  },
                  "photos" : [
                     {
                        "height" : 2268,
                        "html_attributions" : [
                           "\u003ca href=\"https://maps.google.com/maps/contrib/115388526431898128309/photos\"\u003eSittidej Narkviroj\u003c/a\u003e"
                        ],
                        "photo_reference" : "CmRaAAAAZVT-gsFCqeZnRIekNA143CyQQspAAGGUx_9HVD3KyoOr2oAm2glg8aHdiwNzQ66XfGdTe6z4RBIGKixQJJSYwNOeIXuoGs44IjtTiRpy209msRxO7xHzNo63_9B9rxSwEhCJdt1gNh4aF6dKnByHCvXQGhR74Wxxb_7mnvNNMJmaUae-DuIu6g",
                        "width" : 4032
                     }
                  ],
                  "place_id" : "ChIJdY9IpYac4jARYIa1IRcGDQ4",
                  "plus_code" : {
                     "compound_code" : "RGJP+9C กรุงเทพมหานคร",
                     "global_code" : "7P52RGJP+9C"
                  },
                  "price_level" : 2,
                  "rating" : 4.3,
                  "reference" : "ChIJdY9IpYac4jARYIa1IRcGDQ4",
                  "types" : [ "restaurant", "food", "point_of_interest", "establishment" ],
                  "user_ratings_total" : 106
               },
               {
                  "formatted_address" : "829, Tesco Lotus Prachachuen Pracha Rat Road, ซอย 2 แขวง วงศ์สว่าง เขตบางซื่อ กรุงเทพมหานคร 10800 ไทย",
                  "geometry" : {
                     "location" : {
                        "lat" : 13.806355,
                        "lng" : 100.534204
                     },
                     "viewport" : {
                        "northeast" : {
                           "lat" : 13.80782182989272,
                           "lng" : 100.5355847798927
                        },
                        "southwest" : {
                           "lat" : 13.80512217010728,
                           "lng" : 100.5328851201073
                        }
                     }
                  },
                  "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
                  "id" : "f32bf57eef654d220fa0350f813f8c18a50c68ef",
                  "name" : "สีฟ้า Seefah Restaurant สาขาโลตัสประชาชื่น",
                  "opening_hours" : {
                     "open_now" : true
                  },
                  "photos" : [
                     {
                        "height" : 2448,
                        "html_attributions" : [
                           "\u003ca href=\"https://maps.google.com/maps/contrib/108830346222562309294/photos\"\u003eสีฟ้า Seefah Restaurant สาขาโลตัสประชาชื่น\u003c/a\u003e"
                        ],
                        "photo_reference" : "CmRaAAAA0EMLCBXTP-4tDbNatGP9tzfknAtJgt6o-FV-c8dD4baV5yOfh3N-fWvqkmmyOzj9KClaXrppZoWhQAMKTBMUKEcMhlChYcPRWmKWh0ifRJC6X9USE5La7NRh-418GFJoEhDhPFLH6tzpA1HtOH_0-oWcGhQPzbYUaDezie4XulA8H4zAyjiNKQ",
                        "width" : 3264
                     }
                  ],
                  "place_id" : "ChIJDe_MYnSc4jARq-nMIgAY-f4",
                  "plus_code" : {
                     "compound_code" : "RG4M+GM กรุงเทพมหานคร",
                     "global_code" : "7P52RG4M+GM"
                  },
                  "price_level" : 2,
                  "rating" : 3.9,
                  "reference" : "ChIJDe_MYnSc4jARq-nMIgAY-f4",
                  "types" : [ "restaurant", "food", "point_of_interest", "establishment" ],
                  "user_ratings_total" : 26
               },
               {
                  "formatted_address" : "592 ถนน เตชะวณิช แขวง บางซื่อ เขตบางซื่อ กรุงเทพมหานคร 10800 ประเทศไทย",
                  "geometry" : {
                     "location" : {
                        "lat" : 13.801401,
                        "lng" : 100.534413
                     },
                     "viewport" : {
                        "northeast" : {
                           "lat" : 13.80278112989272,
                           "lng" : 100.5357048798927
                        },
                        "southwest" : {
                           "lat" : 13.80008147010728,
                           "lng" : 100.5330052201073
                        }
                     }
                  },
                  "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
                  "id" : "60220e258fb515a6f991227580e31887e4775a2b",
                  "name" : "ก๋วยจั๋บน้ำข้นสุขลาวัลย์",
                  "opening_hours" : {
                     "open_now" : true
                  },
                  "photos" : [
                     {
                        "height" : 1800,
                        "html_attributions" : [
                           "\u003ca href=\"https://maps.google.com/maps/contrib/100464151946039579513/photos\"\u003eThanphon Liwsakul\u003c/a\u003e"
                        ],
                        "photo_reference" : "CmRaAAAAz6PpC_QmH71iIuRgCVLLPPST7GJ7YW5Ke3V8IRdhdDuwoa0WzKxvytzkQizRNlMA0i35J1QKheyN5O7rjM4weaQH2qfBj3pBaSnqzZ2-Hcog5_d7SAEmdAyLi4eN_-_9EhCvDtxxbA1-Rh1IaVPIIDPMGhSC1CU1XtTa8WDmv8H5IDUcqImgjw",
                        "width" : 3200
                     }
                  ],
                  "place_id" : "ChIJK8TBaAuc4jARWiuG1zUToVU",
                  "plus_code" : {
                     "compound_code" : "RG2M+HQ กรุงเทพมหานคร",
                     "global_code" : "7P52RG2M+HQ"
                  },
                  "rating" : 4.4,
                  "reference" : "ChIJK8TBaAuc4jARWiuG1zUToVU",
                  "types" : [ "restaurant", "food", "point_of_interest", "establishment" ],
                  "user_ratings_total" : 22
               },
               {
                  "formatted_address" : "ซอยหมู่บ้านพิบูลย์ บางซื่อ เลขที่ 1566 21 ซอย ริมคลองประปาฝั่งขวา แขวง วงศ์สว่าง เขตบางซื่อ กรุงเทพมหานคร 10800 ไทย",
                  "geometry" : {
                     "location" : {
                        "lat" : 13.823562,
                        "lng" : 100.510003
                     },
                     "viewport" : {
                        "northeast" : {
                           "lat" : 13.82477932989272,
                           "lng" : 100.5114397798927
                        },
                        "southwest" : {
                           "lat" : 13.82207967010728,
                           "lng" : 100.5087401201073
                        }
                     }
                  },
                  "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
                  "id" : "8267e6ca1300d5d2f3b3a66a95d366c619462ac1",
                  "name" : "สวนอาหารบ้านพิบูลย์",
                  "opening_hours" : {
                     "open_now" : true
                  },
                  "photos" : [
                     {
                        "height" : 540,
                        "html_attributions" : [
                           "\u003ca href=\"https://maps.google.com/maps/contrib/103740709961290705216/photos\"\u003eA Google User\u003c/a\u003e"
                        ],
                        "photo_reference" : "CmRaAAAAcDO0cOlGOxeJIB58EPlfxviiQQOChlQJuvZOV8M-1dl70fj1oywIadjyrvoKjXulJ1vW_Lu3S3iRVog9k1B-CagLmzyy8GT3ibRz7UJrr_irIDDvkcx6AJGTFLPBjFVuEhBUdcqJZM9ib4ZUpCuwgvxuGhRPXo52FYz34jEJdX3DH4T-U2Zr9Q",
                        "width" : 960
                     }
                  ],
                  "place_id" : "ChIJi19s_Z6b4jARnb8nGScaMb8",
                  "plus_code" : {
                     "compound_code" : "RGF6+C2 กรุงเทพมหานคร",
                     "global_code" : "7P52RGF6+C2"
                  },
                  "rating" : 4.1,
                  "reference" : "ChIJi19s_Z6b4jARnb8nGScaMb8",
                  "types" : [ "restaurant", "food", "point_of_interest", "establishment" ],
                  "user_ratings_total" : 19
               },
               {
                  "formatted_address" : "367, 5 ถนน กรุงเทพ - นนทบุรี แขวง บางซื่อ เขตบางซื่อ กรุงเทพมหานคร 10800 ประเทศไทย",
                  "geometry" : {
                     "location" : {
                        "lat" : 13.8110418,
                        "lng" : 100.5312451
                     },
                     "viewport" : {
                        "northeast" : {
                           "lat" : 13.81238772989272,
                           "lng" : 100.5326421298927
                        },
                        "southwest" : {
                           "lat" : 13.80968807010728,
                           "lng" : 100.5299424701073
                        }
                     }
                  },
                  "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
                  "id" : "518b0caf649263329897198c6bef34d120665ca5",
                  "name" : "รสเด็ดเตาปูน",
                  "opening_hours" : {
                     "open_now" : true
                  },
                  "photos" : [
                     {
                        "height" : 640,
                        "html_attributions" : [
                           "\u003ca href=\"https://maps.google.com/maps/contrib/101630529205289786035/photos\"\u003eA Google User\u003c/a\u003e"
                        ],
                        "photo_reference" : "CmRaAAAAmRb6b00ziyIe8EfAfVsTcSvUAClUTu7cx0_zzl-yMn68Ijn6uCWt8s1WRto3GhRvr0nhL7LjL76xz6r937HqPMdE85g3PTKxj3urvZr7m2cRkxs_dE5qdiQU9hpWAd4wEhAFrEDe11aFYKgiijpdtEdsGhRVd9Ia31qDQxN-8jMmCUnnYt12FQ",
                        "width" : 960
                     }
                  ],
                  "place_id" : "ChIJ6ZD3SHac4jARePDT9WBypow",
                  "plus_code" : {
                     "compound_code" : "RG6J+CF กรุงเทพมหานคร",
                     "global_code" : "7P52RG6J+CF"
                  },
                  "rating" : 3.9,
                  "reference" : "ChIJ6ZD3SHac4jARePDT9WBypow",
                  "types" : [ "restaurant", "food", "point_of_interest", "establishment" ],
                  "user_ratings_total" : 25
               },
               {
                  "formatted_address" : "888, Big C Wongsawang, Phibun Songkham Road, Bang Sue, Khet Bang Sue, Bangkok, 10800, ไทย",
                  "geometry" : {
                     "location" : {
                        "lat" : 13.8267455,
                        "lng" : 100.5283678
                     },
                     "viewport" : {
                        "northeast" : {
                           "lat" : 13.82818337989272,
                           "lng" : 100.5296380298927
                        },
                        "southwest" : {
                           "lat" : 13.82548372010728,
                           "lng" : 100.5269383701073
                        }
                     }
                  },
                  "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
                  "id" : "dc93f3fb1658002bd69651540317dd99822c711b",
                  "name" : "MK Restaurants",
                  "opening_hours" : {
                     "open_now" : true
                  },
                  "photos" : [
                     {
                        "height" : 3366,
                        "html_attributions" : [
                           "\u003ca href=\"https://maps.google.com/maps/contrib/104822309547054761050/photos\"\u003eYingyot Srikamonkrid\u003c/a\u003e"
                        ],
                        "photo_reference" : "CmRaAAAAChn0j6o7rF6lB2VHUXo9CJROasgSfXljCvFyll5h16BGebe6Z5nPOHbABz6PCySGV002usnsK9QHdbbcZfEnmsRYBT80WAjLahkYz5FqdAUWuR7hHqVw7vs6zQhbF2fzEhCn05xewu1PuY7-yxrbU5qyGhRyEVjjTNpQpsD6YhTAvrBHkKd1BA",
                        "width" : 5984
                     }
                  ],
                  "place_id" : "ChIJ7SQB83-b4jAR66Em1dydPTA",
                  "plus_code" : {
                     "compound_code" : "RGGH+M8 กรุงเทพมหานคร",
                     "global_code" : "7P52RGGH+M8"
                  },
                  "price_level" : 2,
                  "rating" : 3.4,
                  "reference" : "ChIJ7SQB83-b4jAR66Em1dydPTA",
                  "types" : [ "restaurant", "food", "point_of_interest", "establishment" ],
                  "user_ratings_total" : 8
               },
               {
                  "formatted_address" : "87 ซอย ประชาชื่น 6 แขวง บางซื่อ เขตบางซื่อ กรุงเทพมหานคร 10800 ประเทศไทย",
                  "geometry" : {
                     "location" : {
                        "lat" : 13.8175759,
                        "lng" : 100.5395707
                     },
                     "viewport" : {
                        "northeast" : {
                           "lat" : 13.81889112989272,
                           "lng" : 100.5409268298927
                        },
                        "southwest" : {
                           "lat" : 13.81619147010728,
                           "lng" : 100.5382271701073
                        }
                     }
                  },
                  "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
                  "id" : "01415a0d3f37f80477bc059adb06c75a1dcad289",
                  "name" : "ครัวป้าสมศรี",
                  "opening_hours" : {
                     "open_now" : true
                  },
                  "photos" : [
                     {
                        "height" : 960,
                        "html_attributions" : [
                           "\u003ca href=\"https://maps.google.com/maps/contrib/112969724024254256134/photos\"\u003eครัวป้าสมศรี\u003c/a\u003e"
                        ],
                        "photo_reference" : "CmRaAAAAnv981QCSsVwbc_Mk8fF6h71SmV6Bgpi3--tadThdGEsJdc8515jwTYgtyh5-7yr_4TdKsFI2K6Fq6sD083i1qtyEuoDgXcKT8Tpfxf7mLtaIDTdlQ_j0EiNwKbMO1quFEhCme2FsH4f3oHmz5F1smDdSGhRnqGkn4wDp5pxLl9DHFrNvbuzEeQ",
                        "width" : 1706
                     }
                  ],
                  "place_id" : "ChIJSRcFInuc4jARsRGF-PeDQa0",
                  "plus_code" : {
                     "compound_code" : "RG9Q+2R กรุงเทพมหานคร",
                     "global_code" : "7P52RG9Q+2R"
                  },
                  "price_level" : 1,
                  "rating" : 3.9,
                  "reference" : "ChIJSRcFInuc4jARsRGF-PeDQa0",
                  "types" : [ "restaurant", "food", "point_of_interest", "establishment" ],
                  "user_ratings_total" : 37
               },
               {
                  "formatted_address" : "32 ถนน ริมคลองประปา แขวง บางซื่อ เขตบางซื่อ กรุงเทพมหานคร 10800 ประเทศไทย",
                  "geometry" : {
                     "location" : {
                        "lat" : 13.8171549,
                        "lng" : 100.5375265
                     },
                     "viewport" : {
                        "northeast" : {
                           "lat" : 13.81847032989272,
                           "lng" : 100.5388852798927
                        },
                        "southwest" : {
                           "lat" : 13.81577067010728,
                           "lng" : 100.5361856201073
                        }
                     }
                  },
                  "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
                  "id" : "2e420c76147168f0c02c8857c9550b727a90e6d3",
                  "name" : "Vegetarian Restaurant (ร้านเจ)",
                  "opening_hours" : {
                     "open_now" : true
                  },
                  "photos" : [
                     {
                        "height" : 3024,
                        "html_attributions" : [
                           "\u003ca href=\"https://maps.google.com/maps/contrib/105330642668042560974/photos\"\u003eKatrina Shearon\u003c/a\u003e"
                        ],
                        "photo_reference" : "CmRaAAAAqqPjM7i86b4iu4EXjLf8prSTUmZFTLAwJIxpdKCf9_J36r5pnGVz_q_YCTHj0vMBQVoWz3yEEd8ahzFO0tFgawCDpunZxblUVCR22mA9GEKx-KxUbbAMe5vvvV4BTdjuEhAVaetInWScBB3rElSDthTgGhQw5MFGWOjJaA1GLzDoL1pQQm-qug",
                        "width" : 4032
                     }
                  ],
                  "place_id" : "ChIJa8kfU3qc4jAR0JQPqeue7TM",
                  "plus_code" : {
                     "compound_code" : "RG8Q+V2 กรุงเทพมหานคร",
                     "global_code" : "7P52RG8Q+V2"
                  },
                  "rating" : 4.3,
                  "reference" : "ChIJa8kfU3qc4jAR0JQPqeue7TM",
                  "types" : [ "restaurant", "food", "point_of_interest", "establishment" ],
                  "user_ratings_total" : 15
               },
               {
                  "formatted_address" : "3, 393, 393/3 ถนน ประชาราษฎร์ สาย 2 แขวง บางซื่อ เขตบางซื่อ กรุงเทพมหานคร 10800 ไทย",
                  "geometry" : {
                     "location" : {
                        "lat" : 13.8064872,
                        "lng" : 100.5211238
                     },
                     "viewport" : {
                        "northeast" : {
                           "lat" : 13.80780277989272,
                           "lng" : 100.5224728798927
                        },
                        "southwest" : {
                           "lat" : 13.80510312010728,
                           "lng" : 100.5197732201073
                        }
                     }
                  },
                  "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
                  "id" : "3164ea94ea39c4e304419a2468df13730aea0500",
                  "name" : "จั๊กหน่อย",
                  "opening_hours" : {
                     "open_now" : true
                  },
                  "photos" : [
                     {
                        "height" : 2240,
                        "html_attributions" : [
                           "\u003ca href=\"https://maps.google.com/maps/contrib/114175437352757763172/photos\"\u003eSorrasak Pattarapaisith\u003c/a\u003e"
                        ],
                        "photo_reference" : "CmRZAAAADvGcsVz2ftf6B3iRjU9JTZomZyoiuMMfToa_9I3hdKY8HFj-TlhlDRe_yL_HOFlIPQAY8uEHNTPzV-rbUJ1sZ2-oHhwKNo7n0GlbDhK3gViETg5k-0p8TgdoqxKhVoEFEhAviWOIZxWgUNF7jFTyjLxXGhT5Gdxlyu5717g_-EkETAbyNExOrw",
                        "width" : 3968
                     }
                  ],
                  "place_id" : "ChIJ-yZhqo2b4jARsYKjNsLniIw",
                  "plus_code" : {
                     "compound_code" : "RG4C+HC กรุงเทพมหานคร",
                     "global_code" : "7P52RG4C+HC"
                  },
                  "rating" : 4.3,
                  "reference" : "ChIJ-yZhqo2b4jARsYKjNsLniIw",
                  "types" : [ "restaurant", "food", "point_of_interest", "establishment" ],
                  "user_ratings_total" : 59
               },
               {
                  "formatted_address" : "175 ถนน กรุงเทพ- นนทบุรี แขวง บางซื่อ เขตบางซื่อ กรุงเทพมหานคร 10800 ประเทศไทย",
                  "geometry" : {
                     "location" : {
                        "lat" : 13.8074467,
                        "lng" : 100.5307669
                     },
                     "viewport" : {
                        "northeast" : {
                           "lat" : 13.80878627989272,
                           "lng" : 100.5320575298927
                        },
                        "southwest" : {
                           "lat" : 13.80608662010728,
                           "lng" : 100.5293578701073
                        }
                     }
                  },
                  "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
                  "id" : "6498629b592bcee394e83e9b11b467da1d2db9ce",
                  "name" : "นิวกิ๊ดกี่โภชนา",
                  "opening_hours" : {
                     "open_now" : true
                  },
                  "place_id" : "ChIJVXG_lHWc4jARwxQgdeAXEqQ",
                  "plus_code" : {
                     "compound_code" : "RG4J+X8 กรุงเทพมหานคร",
                     "global_code" : "7P52RG4J+X8"
                  },
                  "rating" : 3.6,
                  "reference" : "ChIJVXG_lHWc4jARwxQgdeAXEqQ",
                  "types" : [ "restaurant", "food", "point_of_interest", "establishment" ],
                  "user_ratings_total" : 5
               }
            ],
            "status" : "OK"
         }';
        $xx = 'xxxxx';
        return new ViewModel([
            'xx' => $xx,
            'maps' => $map,
        ]);
    }
}
