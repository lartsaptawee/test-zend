import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import UserIndex from '@/component/Users/Index';

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/users',
      name: 'users',
      component: UserIndex
    }
]
})